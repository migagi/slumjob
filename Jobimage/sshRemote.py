import os
from SSH import SSH

def getResultBatch(host,user,passwd,port,file):
    result = SSH(host=host,user=user,passwd=passwd,port=port).execute('cat /home/'+user+'/'+file+'.out')
    return result
def main():
    host  = os.getenv('host')
    user = os.getenv('user')
    passwd  = os.getenv('passwd')
    port = os.getenv('port')
    uid = os.getenv('uid')
    
    result=getResultBatch(host,user,passwd,port,uid)
    print(result[0])
    return result[2]

if __name__ == "__main__":
    main()
