import kopf
import logging
from SSH import SSH
import pykube
from pykube import  HTTPClient, KubeConfig
from pykube.objects import  NamespacedAPIObject
import base64
import os
from datetime import datetime

class ObjectSecret(Exception):
    """Base class for other exceptions"""
    pass

class NoneUserObjectSecret(Exception):
    """Base class for other exceptions"""
    pass

class NonePassObjectSecret(Exception):
    """Base class for other exceptions"""
    pass

slurmJobs = {}

name=''
namespace=''
host=''
port=''
uid=''
jobCode=''

jobYAML={
    'apiVersion': 'batch/v1',
    'kind': 'Job',
    'metadata':  {'name': 'job'+name,'namespace':namespace},	'annotations:': {'name-slumjob':'pi'},
    'spec': {
        'template': {
            'spec':{
                'containers': [{
                'name': 'slurmjob',
                'image': 'migagi/slumjob-image:1.0.9',
                'command': ["python","sshRemote.py"],
                'volumeMounts':[{'name':'config-volume','mountPath':'/home/config'}],
                'env': [{'name':'user','valueFrom':{'secretKeyRef':{'name':'secretoslum','key':'user'}}},{'name':'passwd','valueFrom':{'secretKeyRef':{'name':'secretoslum','key':'passwd'}}},{'name':'host','value': host},{'name':'port','value': port},{'name':'uid','value': uid}],
                }],
                'volumes': [{ 'name':'config-volume','configMap':{'name':'configmap'+name}}],
                'volumeMounts':[{'name':'config-volume','mountPath':'/home/config'}],
                'restartPolicy': 'Never',
                },
        }
    }
}


def is_it_not_a_timer(record: logging.LogRecord) -> bool:
    txt = record.getMessage()
    return not txt.startswith("Timer ")
@kopf.on.login(retries=3)
def login_fn(**kwargs):
    # Login using the service account that is mounted automatically in the container
    return kopf.login_via_pykube(**kwargs)

@kopf.on.startup()
def hide_timers_logs(**_):
    objlogger = logging.getLogger('kopf.objects')
    objlogger.addFilter(is_it_not_a_timer)

@kopf.on.startup()
def config(settings: kopf.OperatorSettings, logger, **_):
    #OPERATOR_SERVICE_HOST = str(os.getenv('SLURMJOB_OPERATOR_SERVICE_HOST'))
    try:
        #OPERATOR_SERVICE_PORT = int(os.getenv('SLURMJOB_OPERATOR_SERVICE_PORT'))
        OPERATOR_SERVICE_HOST = str(os.getenv('SLURMJOB_OPERATOR_SERVICE_HOST'))
        if os.getenv('SLURMJOB_OPERATOR_SERVICE_HOST') != None :
            print('server')
            logging.info("Cluster WebhookServer")
            settings.admission.server = kopf.WebhookServer(host=OPERATOR_SERVICE_HOST, port=8443)
        else:
            logging.info("Local env WebhookServer")
            settings.admission.server  = kopf.WebhookAutoServer() 
    except:
        logging.error("Error")
        settings.admission.server  = kopf.WebhookAutoServer()
    
    #settings.admission.server = kopf.WebhookServer(host=OPERATOR_SERVICE_HOST,port=8443)
    settings.admission.managed = 'slurmjob.dev'

@kopf.on.validate('slurmjobs', field='spec.connection', operation='CREATE')
def check_numbers_ma(spec,uid,namespace, **_):
    name = spec.get('name')
    connection = spec.get('connection')
    host = connection.get('host')
    port = connection.get('port')
    secretName = connection.get('secretName')
    jobCode = spec.get('job').get('scriptCode')
    if not name:
        raise kopf.AdmissionError(f"Name must be set. Got {name!r}.")
    if not host:
        raise kopf.AdmissionError(f"Host must be set. Got {host!r}.")
    if not port:
        raise kopf.AdmissionError(f"Port must be set. Got {port!r}.")
    if not jobCode:
        raise kopf.AdmissionError(f"Script code must be set. Got {jobCode!r}.")
    if len(jobCode)==0:
        raise kopf.AdmissionError(f"Script code must be set. Got {jobCode!r}.")
    try:
        validate_secret(uid,secretName,namespace)
    except ObjectSecret:
        raise kopf.PermanentError("You must define Secret object with the User and password.")
    except NoneUserObjectSecret:
        raise kopf.PermanentError("You must define User key/value in Secret object.")
    except NonePassObjectSecret:
        raise kopf.PermanentError("You must define Password key/value in Secret object.")


def pykube_client_from_dict():
    try:
        pykube_client = HTTPClient(KubeConfig.from_service_account())
    except:
        pykube_client = pykube.HTTPClient(KubeConfig.from_file(filename='./config'))
    return pykube_client

@kopf.on.create('slurmjobs')
def create_fn(meta,namespace,patch,uid,spec,status,body, **kwargs):
    name = spec.get('name')
    logging.info(f"A handler is called with Object UID: {name}")
    connection = spec.get('connection')
    host = connection.get('host')
    port = connection.get('port')

    logging.info(f"A handler is called with connection: {name}")
    secretName = connection.get('secretName')
    logging.info(f"A handler is called with secretName: {name}")

    jobCode = spec.get('job').get('scriptCode')

    if not name:
        raise kopf.PermanentError(f"Size must be set. Got {name!r}.")
    logging.info(f"A handler is called with namespace: {namespace}")
    logging.info(f"A handler is called with name: {name}")
    if not host:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError(f"Host must be set. Got {host!r}.")
    if not port:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError(f"Port must be set. Got {port!r}.")
    if not jobCode:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError(f"Script code must be set. Got {jobCode!r}.")
    if len(jobCode)==0:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError(f"Script code must be set. Got {jobCode!r}.")
    try:
        validate_secret(uid,secretName,namespace)
    except ObjectSecret:
        #logging.debug(f"A handler is called with namespace: {namespace}")
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError("You must define Secret object with the User and password.")
    except NoneUserObjectSecret:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError("You must define User key/value in Secret object.")
    except NonePassObjectSecret:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError("You must define Password key/value in Secret object.")
    
    api = pykube_client_from_dict()
    secretSlumObj = pykube.Secret.objects(api).filter(namespace=namespace).get(name=secretName)
    api.session.close()
    sSlum=secretSlumObj.obj
    logging.info(f"Get Secret Object : {sSlum}")

    user_bytes = base64.b64decode(sSlum.get('data').get('user'))
    user = user_bytes.decode('ascii')
    passwd_bytes = base64.b64decode(sSlum.get('data').get('passwd'))
    passwd = passwd_bytes.decode('ascii')

    fileName=str(user)+str(uid)+'.sh'
    res=createFile(fileName,uid,jobCode)
    if res:
        logging.info(f"File create in operator POD: {fileName}")
        copyScriptToRemote(host,user,passwd,port,fileName)
        logging.info(f"File copy to slurmServer: {fileName}")
        os.remove(fileName)
        logging.info(f"Remove file from operator POD: {fileName}")
        result=executeScriptBatchRemote(host,user,passwd,port,fileName)
        print(result)
        patch.spec['status'] = 'PENDING'
        slurmjobname=uid
  
    return {'Job-name': result}

def updateColumnstoError(patch):
    patch.spec['status'] = 'ERROR'
    patch.spec['jobid'] = ''
    patch.spec['partition'] = ''
    patch.spec['name'] = ''
    patch.spec['user'] = ''
    patch.spec['time'] = ''

def check(spec, **_):
    connection = spec.get('connection')
    host = connection.get('host')
    port = connection.get('port')
    if not port:
        raise kopf.AdmissionError("port must be a list if present.")

@kopf.on.update('slurmjobs',field='spec')
def update_fn(diff,spec,patch,body, meta,status, namespace,name, logger, **kwargs):
    logging.info(f"A handler update_fn is called with spec: {spec}")
    logging.info(f"A handler diff is called with spec: {diff}")
    status = spec.get('status')
    logging.info(f"A handler status is called with status: {status}")
    try:
        changes=diff[0][1]
    except:
        changes=()
    if 'job' in changes or 'connection' in changes:
        if (status=='ERROR'  or status=='FAILED' or status=='COMPLETED' or status == 'CANCELLED'):
            logging.info(f"Update Error status object is called with status: {status}")
            uid = meta.get('uid')
            logging.info(f"Update object uid: {uid}")
            
            slurmName = name
            connection = spec.get('connection')
            host = connection.get('host')
            port = connection.get('port')
            secretName = connection.get('secretName')
            jobCode = spec.get('job').get('scriptCode')
  
            logging.info(f"A handler is called with namespace: {namespace}")
            if not host:
                raise kopf.PermanentError(f"Host must be set. Got {host!r}.")
            if not port:
                raise kopf.PermanentError(f"Port must be set. Got {port!r}.")
            if not jobCode:
                raise kopf.PermanentError(f"Script code must be set. Got {jobCode!r}.")
            if len(jobCode)==0:
                raise kopf.PermanentError(f"Script code must be set. Got {jobCode!r}.")
            try:
                validate_secret(uid,secretName,namespace)
            except ObjectSecret:
                #logging.debug(f"A handler is called with namespace: {namespace}")
                patch.spec['status'] = 'ERROR'
                raise kopf.PermanentError("You must define Secret object with the User and password.")
            except NoneUserObjectSecret:
                patch.spec['status'] = 'ERROR'
                raise kopf.PermanentError("You must define User key/value in Secret object.")
            except NonePassObjectSecret:
                patch.spec['status'] = 'ERROR'
                raise kopf.PermanentError("You must define Password key/value in Secret object.")

            api = pykube_client_from_dict()
            secretSlumObj = pykube.Secret.objects(api).filter(namespace=namespace).get(name=secretName)
            api.session.close()
            sSlum=secretSlumObj.obj
            logging.info(f"Get Secret Object : {sSlum}")

            user_bytes = base64.b64decode(sSlum.get('data').get('user'))
            user = user_bytes.decode('ascii')
            passwd_bytes = base64.b64decode(sSlum.get('data').get('passwd'))
            passwd = passwd_bytes.decode('ascii')
            #Only try to delete de Pod with the result if their stattus is COMPLETEDD 
            if  status=='COMPLETED':
                try:
                    #slurmJobs[uid].update(jobId = None)
                    #slurmJobs[uid].update(status = 'PENDING')
                    api = pykube.HTTPClient(pykube.KubeConfig.from_env())
                    PodObject = pykube.Pod.objects(api).filter(namespace=namespace).get_by_name(slurmName)
                    logger.info(f"Pod will be deleted ... : {PodObject}")
                    PodObject.delete()
                    api.session.close()
                except Exception as e:
                    logger.error(f"Error getting status Pod. {e}")
            
            try:
                fileName=str(user)+str(uid)+'.sh'
                res=createFile(fileName,uid,jobCode)
                if res:
                    logging.info(f"File create in operator POD: {fileName}")
                    copyScriptToRemote(host,user,passwd,port,fileName)
                    logging.info(f"File copy to slurmServer: {fileName}")
                    os.remove(fileName)
                    logging.info(f"Remove file from operator POD: {fileName}")
                    result=executeScriptBatchRemote(host,user,passwd,port,fileName)
                    logging.info(f"update executeScriptBatchRemote: {result}")
            except:
                #slurmJobs[uid].update(status = 'FAILED')
                patch.spec['status'] = 'FAILED'
            try:
                patch.spec['status'] = 'PENDING'
                patch.spec['jobid'] = None
                del slurmJobs[uid]
            except:
                logging.info("Error deleted slurmJobs")
        else:
            logging.info(f" changes in others parts than in spec: {diff}") 

@kopf.on.delete('slurmjobs')
def my_handler(spec,uid,status, **_):
    try:
        status = spec.get('status')
        logging.info(f"kopf.on.delete status: {status}")
        if status != 'COMPLETED' or  status !=  'CANCELLED' or  status !=  'ERROR':
            connection = spec.get('connection')
            host = connection.get('host')
            port = connection.get('port')
            user = slurmJobs[uid].get('user')
            passwd = slurmJobs[uid].get('passwd')
            jobId = slurmJobs[uid].get('jobId')
            executeSCANCEL(host,user,passwd,port,jobId)
        logging.info(f"kopf.on.delete  JobID {slurmJobs[uid].get('jobId')}")
        del slurmJobs[uid]
    except Exception as e:
        logging.error(f" ERROR kopf.on.delete: {e}")
    pass

@kopf.on.timer('slurmjobs',interval=1)
async def monitor_kex(patch,stopped,namespace, name,logger, body,uid, spec   , **kwargs):
    user = ""
    passwd = ""
    connection = spec.get('connection')
    slurmName = name
    status = spec.get('status')

    logging.info(f"kopf.on.timer  UID {uid}")
    logging.info(f"kopf.on.timer  status {status}")

    host = connection.get('host')
    port = connection.get('port')
    secretName = connection.get('secretName')
    jobid = spec.get('jobid')

    #cache object
    if uid not in slurmJobs:

        logging.info(f"kopf.on.timer  Object NOT in cache: {jobid}")
        api = pykube_client_from_dict()
        secretSlumObj = pykube.Secret.objects(api).filter(namespace=namespace).get(name=secretName)
        api.session.close()
        sSlum=secretSlumObj.obj
        user_bytes = base64.b64decode(sSlum.get('data').get('user'))
        user = user_bytes.decode('ascii')
        passwd_bytes = base64.b64decode(sSlum.get('data').get('passwd'))
        passwd = passwd_bytes.decode('ascii')

        logging.debug(f"kopf.on.timer  decode user and pass {user}:{passwd}")
        slurmJobs[uid] = {'user':user,'passwd':passwd}
        if status:
            slurmJobs[uid].update(status = status)
        if jobid:
            slurmJobs[uid].update(jobid = jobid)
    else:
        logging.info(f"From cache timer: {slurmJobs[uid]}")
        user = slurmJobs[uid].get('user')
        passwd = slurmJobs[uid].get('passwd')
    
    if (slurmJobs[uid].get('status') != 'ERROR' and slurmJobs[uid].get('status') != 'FAILED' and slurmJobs[uid].get('status') != 'COMPLETED' and slurmJobs[uid].get('status') != 'CANCELLED' and slurmJobs[uid].get('status') != 'GETTING RESULT') or slurmJobs[uid].get('status')==None:
        logging.debug(f"SSH PARAMETERS User {user} passwd {passwd} host {host} port {port} ")
        dt = datetime.now()
        # getting the timestamp
        ts = datetime.timestamp(dt)
        date_time = datetime.fromtimestamp(ts)
        patch.spec['timestamp'] = str(date_time)
        try:
            result = executeScriptRemote(host,user,passwd,port,uid)
            print(result)
            if len(result)>0:
                try:
                    #result.pop()[4]
                    lastResult=result.pop()
                    statusSlurmJob=lastResult[4]
                    jobId = lastResult[0]
                    partition = lastResult[1]
                    name = lastResult[2]
                    userSlurm = lastResult[3]
                    time = lastResult[5]

                    logging.info(f"SSH Remote Server Status: {statusSlurmJob} ")
                    if statusSlurmJob:
                        logging.info(f"JobID from Cache: {slurmJobs[uid]} ")
                        if slurmJobs[uid].get('jobId')==None or slurmJobs[uid].get('jobId')==jobId:
                            patch.spec['status'] = statusSlurmJob
                            slurmJobs[uid].update(status = statusSlurmJob)
                            slurmJobs[uid].update(jobId = jobId)
                            if statusSlurmJob=='FAILED':
                                slurmJobs[uid].update(jobId = jobId)
                            elif statusSlurmJob=='COMPLETED':
                                patch.spec['status'] = 'GETTING RESULT'
                                slurmJobs[uid].update(status = 'GETTING RESULT')
                                # Define generic Pod
                                pod_data={
                                    'apiVersion': 'v1',
                                    'kind': 'Pod',
                                    'metadata':{'name':'slurmName'},
                                    'spec': {
                                                'containers': [{
                                                'name': 'slumjob-demo-container',
                                                'image': 'migagi/slumjob-image:1.0.12',
                                                'command': ["python","sshRemote.py"],
                                                'env': [{'name':'user','valueFrom':{'secretKeyRef':{'name':'secretoslum','key':'user'}}},{'name':'passwd','valueFrom':{'secretKeyRef':{'name':'secretoslum','key':'passwd'}}},{'name':'host','value': host},{'name':'port','value': port},{'name':'uid','value': uid}],
                                                }],
                                                'restartPolicy': 'Never',
                                    }
                                }
                                logging.info(f"slurmName: {slurmName} ")
                                pod_data['metadata']['name']=slurmName
                                pod_data['spec']['containers'][0]['name']=slurmName
                                pod_data['spec']['containers'][0]['env'][2]['value']=host
                                pod_data['spec']['containers'][0]['env'][3]['value']=port
                                pod_data['spec']['containers'][0]['env'][4]['value']=uid
                                
                                patch.spec['jobid'] = jobId
                                patch.spec['partition'] = partition
                                patch.spec['name'] = name
                                patch.spec['user'] = userSlurm
                                patch.spec['time'] = time

                                logging.info(f"Creating pod : {slurmName} ")
                                kopf.adopt(pod_data, nested='spec.template')
                                kopf.label(jobYAML, {'slurmname': slurmName })

                                print(pod_data)
                                logging.debug(f"pod_data: {pod_data} ")
                                api = pykube.HTTPClient(pykube.KubeConfig.from_env())
                                pod_data_pykube = pykube.objects.Pod(api, pod_data)
                                api.session.close()
                                pod_data_pykube.create()
                                logging.debug(f"pod_data_pykube: {pod_data_pykube} ")
                            else:
                                logging.info(f"State get from remote server is: {statusSlurmJob} ")
                                patch.spec['status'] = statusSlurmJob
                                patch.spec['jobid'] = jobId
                                patch.spec['partition'] = partition
                                patch.spec['name'] = name
                                patch.spec['user'] = userSlurm
                                patch.spec['time'] = time
                                
                except Exception as e:
                    logging.error(f"SSH EXCEPTION Result: {e} ")
                    logging.error(f"SSH DATA FROM REMOTE SERVER: {result} ")
        except Exception as e:
            patch.spec['status'] = 'FAILED'
            slurmJobs[uid].update(status = 'FAILED')
    else:
        if slurmJobs[uid].get('status') == 'FAILED' and status !='FAILED':
            patch.spec['status'] = 'FAILED'
        elif slurmJobs[uid].get('status') =='GETTING RESULT':
            try:
                logger.info(f"GETTING RESULT")
                dt = datetime.now()
                # getting the timestamp
                ts = datetime.timestamp(dt)
                date_time = datetime.fromtimestamp(ts)
                patch.spec['timestamp'] = str(date_time)
                api = pykube.HTTPClient(pykube.KubeConfig.from_env())
                PodObject = pykube.Pod.objects(api).filter(namespace=namespace).get_by_name(slurmName)
                logger.info(f"POD Created ... : {PodObject}")
                api.session.close()
                if PodObject.obj['status']['phase']== 'Succeeded':
                    patch.spec['status'] = 'COMPLETED'
                    slurmJobs[uid].update(status = 'COMPLETED')
            except Exception as e:
                logger.error(f"Error getting status Pod. {e}")
            #print(JobObject.namespace, JobObject.name,JobObject.obj["spec"]['status'])
        logging.info(f"SSH WITH STATUS FAILED OR COMPLETEDD")

def validate_secret(uid,secretName,namespace):
    api = pykube_client_from_dict()
    print('AQUI')
    print(uid) 
    print(namespace)
    print(secretName)
    try:
        secretSlumObj = pykube.Secret.objects(api).filter(namespace=namespace).get(name=secretName)
        api.session.close()
    except Exception as e:
        print('peto '+str(e))      
        raise ObjectSecret
    print('secretSlumObj')

    sSlum=secretSlumObj.obj
    #Si no existe lanzar error
    if sSlum.get('data').get('user') == None:
        print('NoneUserObjectSecret')
        raise NoneUserObjectSecret
    #Si no existe lanzar error
    if sSlum.get('data').get('passwd') == None:
        print('NonePassObjectSecret')
        raise NonePassObjectSecret

    #print(f'XXXXXX> secretName: {sSlum.get("data").get("user")}')
    #print(json.dumps(    secretSlumObj.obj,    sort_keys=True,    indent=4,    separators=(',', ': ')    ))
def createFile(fileName,uid,jobCode):
    try:
        f = open(fileName, "w")
        f.write("#!/bin/bash\n")
        f.write("#SBATCH -J "+uid+"\n")
        f.write('#SBATCH --output='+uid+'.out\n')
        f.write('#SBATCH --error='+uid+'.err\n')
        f.write(jobCode)
        f.close()
        os.system('chmod +x '+fileName)
        return True
    except:
        return False 
        
def executeScriptBatchRemote(host,user,passwd,port,file):
    print(host,user,passwd,port,file)
    result = SSH(host=host,user=user,passwd=passwd,port=port).execute('chmod +x /home/'+user+'/'+file)
    print(f'chmod +x /home/: {result}')
    print(result)
    #SBATCH  sbatch script.sh
    result = SSH(host=host,user=user,passwd=passwd,port=port).execute('sbatch /home/'+user+'/'+file)
    return result
def getResultBatch(host,user,passwd,port,file):
    print(host,user,passwd,port,file)
    result = SSH(host=host,user=user,passwd=passwd,port=port).execute('cat /home/'+user+'/'+file+'.out')
    print(f'Resutado {result}')
    print(result)
    return result
def executeScriptRemote(host,user,passwd,port,jobName):
    print(host,user,passwd,port,jobName)
    #SBATCH 
    #squeue --format="%.18i %.9P %.30j %.8u %.8T %.10M " -n migagi-1 -t all squeue --format="%.18i;%.9P;%.30j;%.8u;%.8T;%.10M;" -n MIGAGI-JOB -t all
    result = SSH(host=host,user=user,passwd=passwd,port=port).execute('squeue --format="%.18i;%.9P;%.40j;%.8u;%.9T;%.10M;" -n '+jobName+' -t all --sort=+i',timeout=1)
    status=result[2]
    if status == 0:
        return parse_squeue(result[0])
    else : return[]
def executeSCANCEL(host,user,passwd,port,jobid):
    print('executeSCANCEL',host,user,passwd,port,jobid)
    result = SSH(host=host,user=user,passwd=passwd,port=port).execute('scancel '+jobid,timeout=1)
def copyScriptToRemote(host,user,passwd,port,file):
    d = SSH(host=host,user=user,passwd=passwd,port=port).sftp_put(file,'./'+file)
# This function facilitates the parsing of the squeue command exit
def parse_squeue(out):
    #out = out.decode()
    #if out.find("=") < 0: return []
    r = []
    for line in out.split("\n"):
        split_val = line.replace(' ','').split(';')
        #print(split_val)
        r.append(split_val)
    r.pop(0)
    r.pop()
    return r