from email.policy import default
from plistlib import UID
import kopf
import logging
from SSH import SSH
import pykube
from pykube.objects import  NamespacedAPIObject
import json
import asyncio
import time
import base64

class ObjectSecret(Exception):
    """Base class for other exceptions"""
    pass

class NoneUserObjectSecret(Exception):
    """Base class for other exceptions"""
    pass

class NonePassObjectSecret(Exception):
    """Base class for other exceptions"""
    pass

slurmJobs = {}

name=''
namespace=''
host=''
port=''
uid=''
jobCode=''

jobYAML={
    'apiVersion': 'batch/v1',
    'kind': 'Job',
    'metadata':  {'name': 'job'+name,'namespace':namespace},	'annotations:': {'name-slumjob':'pi'},
    'spec': {
        'template': {
            'spec':{
                'containers': [{
                'name': 'slurmjob'+name,
                'image': 'migagi/slumjob-image:1.0.9',
                'command': ["python","sshRemote.py"],
                'volumeMounts':[{'name':'config-volume','mountPath':'/home/config'}],
                'env': [{'name':'user','valueFrom':{'secretKeyRef':{'name':'secretoslum','key':'user'}}},{'name':'passwd','valueFrom':{'secretKeyRef':{'name':'secretoslum','key':'passwd'}}},{'name':'host','value': host},{'name':'port','value': port},{'name':'uid','value': uid}],
                }],
                'volumes': [{ 'name':'config-volume','configMap':{'name':'configmap'+name}}],
                'volumeMounts':[{'name':'config-volume','mountPath':'/home/config'}],
                'restartPolicy': 'Never',
                },
        }
    }
}



configMapJob={'apiVersion': 'v1',
                'kind': 'ConfigMap',
                'metadata': {'name': 'configmap'+name, 'namespace':namespace},
                'data': {
                'script.sh': jobCode }}



# pykube imports:
try:
    from pykube.config import KubeConfig
    from pykube.http import HTTPClient
    from pykube.objects import (
        Job,
        Pod
    )
except ImportError as exc:
    KubeConfig = None
    print ('The Python pykube package is required to use '
           'this feature, please install it or correct the '
           'following error:\nImportError %s' % str(exc))
@kopf.on.login(retries=3)
def login_fn(**kwargs):
    # Login using the service account that is mounted automatically in the container
    return kopf.login_via_pykube(**kwargs)

def pykube_client_from_dict():

    #pykube_client = HTTPClient(KubeConfig.from_service_account())
    pykube_client = pykube.HTTPClient(KubeConfig.from_file(filename='./config'))
    logging.info(f"...................... ...: ")
    return pykube_client

@kopf.on.create('slumjobs')
def create_fn(meta,namespace,patch,uid,spec,status,body, **kwargs):
#    print(f' Kwargs: {kwargs}' )
    api = pykube_client_from_dict()
    print(f" Meta: {meta.get('uid')}" )
#    print(json.dumps(meta,sort_keys=True,indent=4,separators=(',', ': ')))
    #uid = meta.get('uid')
    name = spec.get('name')
    connection = spec.get('connection')
    host = connection.get('host')
    port = connection.get('port')

    print(f' connection: {connection}' )

    secretName = connection.get('secretName')
    print(f'XXXXXX> secretName: {secretName}' )

    jobCode = spec.get('job').get('scriptCode')

    if not name:
        raise kopf.PermanentError(f"Size must be set. Got {name!r}.")
    logging.info(f"A handler is called with namespace: {namespace}")
    logging.info(f"A handler is called with name: {name}")
    if not host:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError(f"Host must be set. Got {host!r}.")
    if not port:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError(f"Port must be set. Got {port!r}.")
    if not jobCode:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError(f"Script code must be set. Got {jobCode!r}.")
    if len(jobCode)==0:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError(f"Script code must be set. Got {jobCode!r}.")
    try:
        validate_secret(uid,secretName,namespace)
    except ObjectSecret:
        #logging.debug(f"A handler is called with namespace: {namespace}")
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError("You must define Secret object with the User and password.")
    except NoneUserObjectSecret:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError("You must define User key/value in Secret object.")
    except NonePassObjectSecret:
        patch.spec['status'] = 'ERROR'
        raise kopf.PermanentError("You must define Password key/value in Secret object.")
    
    
    '''
        configMapJob={'apiVersion': 'v1',
                  'kind': 'ConfigMap',
                  'metadata': {'name': 'configmap'+name, 'namespace':namespace},
                  'data': {
                  'script.sh': jobCode }}
    '''


    configMapJob['metadata']['name']= 'configmap'+name
    configMapJob['metadata']['namespace']=namespace
    configMapJob['data']['script.sh']=jobCode

    kopf.adopt(configMapJob)
    kopf.label(configMapJob, {'application': 'slumjobs','name':'configmap'+name })
    kopf.append_owner_reference(configMapJob)
    configMaoKubejob = pykube.objects.ConfigMap(api, configMapJob)
    configMaoKubejob.create()
    

    kopf.info(body, reason='ConfigMap', message='ConfigMap with script create.')
    patch.spec['status'] = 'PENDING'
    slurmjobname=uid
    '''
    jobYAML={
        'apiVersion': 'batch/v1',
        'kind': 'Job',
        'metadata':  {'name': 'job'+name,'namespace':namespace},	'annotations:': {'name-slumjob':'pi'},
        'spec': {
            'template': {
                'spec':{
                    'containers': [{
                    'name': 'slumjob',
                    'image': 'migagi/slumjob-image:1.0.9',
                    'command': ["python","sshRemote.py"],
                    'volumeMounts':[{'name':'config-volume','mountPath':'/home/config'}],
                    'env': [{'name':'user','valueFrom':{'secretKeyRef':{'name':'secretoslum','key':'user'}}},{'name':'passwd','valueFrom':{'secretKeyRef':{'name':'secretoslum','key':'passwd'}}},{'name':'host','value': host},{'name':'port','value': port},{'name':'uid','value': uid}],
                    }],
                    'volumes': [{ 'name':'config-volume','configMap':{'name':'configmap'+name}}],
                    'volumeMounts':[{'name':'config-volume','mountPath':'/home/config'}],
                    'restartPolicy': 'Never',
                    },
            }
        }
    }
    '''

    jobYAML['spec']['template']['spec']['containers'][0]['name']='slurmjob'+uid
    jobYAML['metadata']['name']='job'+name
    jobYAML['metadata']['namespace']=namespace
    jobYAML['spec']['template']['spec']['containers'][0]['env'][0]['valueFrom']['secretKeyRef']['name']=secretName
    jobYAML['spec']['template']['spec']['containers'][0]['env'][1]['valueFrom']['secretKeyRef']['name']=secretName
    jobYAML['spec']['template']['spec']['containers'][0]['env'][2]['value']=host
    jobYAML['spec']['template']['spec']['containers'][0]['env'][3]['value']=port
    jobYAML['spec']['template']['spec']['containers'][0]['env'][4]['value']=uid
    jobYAML['spec']['template']['spec']['volumes'][0]['configMap']['name']='configmap'+name

    kopf.adopt(jobYAML)
    kopf.label(jobYAML, {'application': 'slumjobs','name':name })
    kopf.info(body, reason='Job', message='Job create.')
    job = pykube.objects.Job(api, jobYAML )
    job.create()
    
    #print(f' status: {status}')
    #print('........................................')
    #print(job.status)
    print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX........................................')
    print(job.metadata)
    #print(job.objects)
    print('........................................')

    #for pod in Pod.objects(api).filter(namespace=namespace, selector={'job-name': 'pi'}):
    #    print('#####################')
    #    print(pod.namespace, pod.name)
    #JobObject = pykube.Job.objects(api).filter(namespace="slum").get(name="pi")
    #logger.info(f"Job Created ... : {obj}")
    api.session.delete()
    return {'Job-name': job.name}

def check(spec, **_):
    connection = spec.get('connection')
    host = connection.get('host')
    port = connection.get('port')
    if not port:
        raise kopf.AdmissionError("port must be a list if present.")

@kopf.on.update('slumjobs',field='spec')
def update_fn(diff,spec,patch,body, meta,status, namespace, logger, **kwargs):
    logging.info(f"A handler update_fn is called with spec: {spec}")
    logging.info(f"A handler diff is called with spec: {diff}")    
    status = spec.get('status')

    try:
        changes=diff[0][1]
    except:
        changes=()

    if 'job' in changes or 'connection' in changes:
        if (status=='FAILED' or status=='COMPLETE' ):
            api = pykube_client_from_dict()
            logging.info(f"Update Error status object is called with status: {status}")
        #    print(json.dumps(meta,sort_keys=True,indent=4,separators=(',', ': ')))
            uid = meta.get('uid')
            name = spec.get('name')
            connection = spec.get('connection')
            host = connection.get('host')
            port = connection.get('port')

            print(f' connection: {connection}' )

            secretName = connection.get('secretName')
            print(f'XXXXXX> secretName: {secretName}' )

            jobCode = spec.get('job').get('scriptCode')

            if not name:
                raise kopf.PermanentError(f"Size must be set. Got {name!r}.")
            logging.info(f"A handler is called with namespace: {namespace}")
            logging.info(f"A handler is called with name: {name}")
            if not host:
                raise kopf.PermanentError(f"Host must be set. Got {host!r}.")
            if not port:
                raise kopf.PermanentError(f"Port must be set. Got {port!r}.")
            if not jobCode:
                raise kopf.PermanentError(f"Script code must be set. Got {jobCode!r}.")
            if len(jobCode)==0:
                raise kopf.PermanentError(f"Script code must be set. Got {jobCode!r}.")
            try:
                validate_secret(uid,secretName,namespace)
            except ObjectSecret:
                #logging.debug(f"A handler is called with namespace: {namespace}")
                patch.spec['status'] = 'ERROR'
                raise kopf.PermanentError("You must define Secret object with the User and password.")
            except NoneUserObjectSecret:
                patch.spec['status'] = 'ERROR'
                raise kopf.PermanentError("You must define User key/value in Secret object.")
            except NonePassObjectSecret:
                patch.spec['status'] = 'ERROR'
                raise kopf.PermanentError("You must define Password key/value in Secret object.")           
            kopf.remove_owner_reference(configMapJob)
            #borro ConfigMap con el antiguo codigo
            configMaoKubejob = pykube.objects.ConfigMap(api, configMapJob )
            configMaoKubejob.delete()

            while configMaoKubejob.exists():
                time.sleep(4)
                logging.info(f"waiting for configMaoKubejob object be deleted")
                kopf.info(body, reason='Job', message='waiting for configMaoKubejob object be deleted.')

            #borro JOB
            #kopf.adopt(configMapJob)
            #kopf.label(configMapJob, {'application': 'slumjobs','name':name })
            kopf.remove_owner_reference(jobYAML)
            job = pykube.objects.Job(api, jobYAML )
            job.delete()
            print('XXXXX')
            print(job.exists())

            while job.exists():
                time.sleep(1)
                logging.info(f"waiting for job object be deleted")

            #borro pods si no los deja huerfanos
            for pod in Pod.objects(api).filter(namespace=pykube.all, selector={'job-name': 'job'+name}):
                print('------------------------')
                print(pod.namespace, pod.name)
                pod.delete()
            time.sleep(5)
            configMapJob['metadata']['name']= 'configmap'+name
            configMapJob['metadata']['namespace']=namespace
            configMapJob['data']['script.sh']=jobCode
            
            kopf.adopt(configMapJob)
            kopf.label(configMapJob, {'application': 'slumjobs','name':name })
            configMaoKubejob = pykube.objects.ConfigMap(api, configMapJob )
            configMaoKubejob.create()

            jobYAML['spec']['template']['spec']['containers'][0]['name']='slurmjob'+uid
            jobYAML['metadata']['name']='job'+name
            jobYAML['metadata']['namespace']=namespace
            jobYAML['spec']['template']['spec']['containers'][0]['env'][0]['valueFrom']['secretKeyRef']['name']=secretName
            jobYAML['spec']['template']['spec']['containers'][0]['env'][1]['valueFrom']['secretKeyRef']['name']=secretName
            jobYAML['spec']['template']['spec']['containers'][0]['env'][2]['value']=host
            jobYAML['spec']['template']['spec']['containers'][0]['env'][3]['value']=port
            jobYAML['spec']['template']['spec']['containers'][0]['env'][4]['value']=uid
            jobYAML['spec']['template']['spec']['volumes'][0]['configMap']['name']='configmap'+name

            kopf.adopt(jobYAML)
            kopf.label(jobYAML, {'application': 'slumjobs','name':name })
            job = pykube.objects.Job(api, jobYAML )
            job.create()

            kopf.info(body, reason='Job', message='Job create.')
            #time.sleep(40)
            slurmJobs[uid].update(status = 'PENDING')
            patch.spec['status'] = 'PENDING'
            #del  slurmJobs[uid]['jobId']
            kopf.info(body, reason='Job', message='He esperado 40 seg?.')
            logging.info(f"----------------------------: {patch}")
        else:
            logging.info(f" changes in others parts than in spec: {diff}") 
@kopf.on.delete('slumjobs')
def my_handler(spec,status, **_):
    logging.info(f" kopf.on.delete  {status}")
    pass

@kopf.on.event('job', labels={'application': 'slumjobs'})
def my_handler(event, **_):
    api = pykube_client_from_dict()
    type=event.get('type')
    #logging.info(f"A handler JOB update_fn is called with spec: {event}")    
    #print(json.dumps(    event,    sort_keys=True,    indent=4,    separators=(',', ': ')    ))
    logging.info(f" TTTTTTTTTTTTTTTTTTTTTTTTTTTT  {event.get('object').get('status')}")
    logging.info(f" LLLLLLLLLLLLLLLLLLLZZZZZZZZZ  {event.get('object').get('metadata').get('labels')}")
    name = event.get('object').get('metadata').get('labels').get('name')
    status = event.get('object').get('status')
    #Busco la definicion del CRD en el object_factory
    SlumJob = pykube.object_factory(api, "kopf.dev/v1", "SlumJob")
    #slj = SlumJob.objects(api).filter(namespace=pykube.all).get(name=name)
    
    for crd in SlumJob.objects(api).filter(namespace=pykube.all):
    #for pod in SlumJob.objects(api).get(name=name):
       sljName = crd.obj["spec"]['name']
       #ID DEL TRABAJO
       print(sljName)
       if sljName == name:
#          print(json.dumps(    pod.obj,    sort_keys=True,    indent=4,    separators=(',', ': ')    ))    
          print(crd.obj["spec"]['status'])
          print(crd.namespace, crd.name)
          #print(status[failed])
          #if status.get('failed') is not None and status.get('failed') > 0:
          #    crd.obj["spec"]['status'] = 'JOB FAILED'
          #    crd.update()
          #    if status.get("conditions") is not None and  status.get("conditions")[0].get("message"):
          #       kopf.info(crd, reason='Job', message=status.get("conditions")[0].get("message"))
          #    else:
          #       kopf.info(crd, reason='Job', message='JOB FAILED. RETRYING '+str(status.get('failed')))
              
#deploy.obj["spec"]["template"]["spec"]["containers"][0]["image"] = new_docker_image
#deploy.update()

    
#API Version:  kopf.dev/v1
#Kind:         SlumJob

#    a= pykube.Objects.objects(api).get(namespace=pykube.all,name="slumjobs.kopf.dev")
#    print(a)
#    Query(api, Objects.CustomResourceDefinition).filter(selector="application=foo").execute();
#    for slumjob in pykube.Object.objects(api).get('my-slumjob'):
#        print("ssss")


#@kopf.on.event('slumjobs')
#def my_handler(event, **_):
#    logging.info(f"A ------------ SLUMJOB ------- update_fn is called with spec: {event}")

@kopf.on.timer('slumjobs',idle=10,interval=5)
async def monitor_kex(patch,stopped,namespace, logger, body,uid, spec   , **kwargs):

    # return {"foo": True}
    print("------------> timer "+uid)
    user = ""
    passwd = ""
    #kopf.label(body, {'application': 'nomodificar','name':'name' })
    #executeScriptRemote(host,user,passwd,port,file)
    connection = spec.get('connection')
    name = spec.get('name')
    status = spec.get('status')
    host = connection.get('host')
    port = connection.get('port')
    secretName = connection.get('secretName')
    print(f' secretName: {secretName}' )
    print('EEEEEEEEEEEEEEEE')    
    print(uid)
    print(slurmJobs)

    #cache object
    if uid not in slurmJobs:
        logging.info(f"CALL pykube_client_from_dict:")
        api = pykube_client_from_dict()
        secretSlumObj = pykube.Secret.objects(api).filter(namespace=namespace).get(name=secretName)
        api.session.close()
        sSlum=secretSlumObj.obj
        logging.info(f"CALL pykube_client_from_dict: {sSlum}")

        user_bytes = base64.b64decode(sSlum.get('data').get('user'))
        user = user_bytes.decode('ascii')
        passwd_bytes = base64.b64decode(sSlum.get('data').get('passwd'))
        passwd = passwd_bytes.decode('ascii')
        print(f" user: {user}" )
        print(f" passwd: {passwd}" )
        slurmJobs[uid] = {'user':user,'passwd':passwd}
    else:
        logging.info(f"From cache timer: {slurmJobs[uid]}")
        user = slurmJobs[uid].get('user')
        passwd = slurmJobs[uid].get('passwd')
    
    if (slurmJobs[uid].get('status') != 'FAILED' and slurmJobs[uid].get('status') != 'COMPLETE') or slurmJobs[uid].get('status')==None:
        logging.info(f"SSH PARAMETERS User {user} passwd {passwd} host {host} port {port} ")
        try:
            result = executeScriptRemote(host,user,passwd,port,uid)

            if len(result)>0:
                try:
                    #result.pop()[4]
                    lastResult=result.pop()
                    statusSlurmJob=lastResult[4]
                    jobId = lastResult[0]
                    #result[0][4]
                    logging.info(f"SSH PARAMETERS statusSlurmJob: {statusSlurmJob} ")
                    if statusSlurmJob:
                        if slurmJobs[uid].get('jobId')==None or slurmJobs[uid].get('jobId')!=jobId:
                            patch.spec['status'] = statusSlurmJob
                            slurmJobs[uid].update(status = statusSlurmJob)
                            if statusSlurmJob=='FAILED' or statusSlurmJob=='COMPLETE':
                                slurmJobs[uid].update(jobId = jobId)
                except Exception as e:
                    logging.error(f"SSH PARAMETERS Result: {e} ")
                    logging.error(f"SSH PARAMETERS Result: {result} ")
                    logging.info(f"SSH result: {result} ")
            #patch.status['done'] = True/False
        except Exception as e:
            patch.spec['status'] = 'FAILED'
            slurmJobs[uid].update(status = 'FAILED')
            
    else:
        if slurmJobs[uid].get('status') == 'FAILED' and status !='FAILED':
            patch.spec['status'] = 'FAILED'
        logging.info(f"SSH WITH STATUS FAILED OR COMPLETED")

def validate_secret(uid,secretName,namespace):
    api = pykube_client_from_dict()
    print('AQUI')
    print(uid) 
    print(namespace)
    print(secretName)
    try:
        secretSlumObj = pykube.Secret.objects(api).filter(namespace=namespace).get(name=secretName)
        api.session.close()
    except Exception as e:
        print('peto '+str(e))      
        raise ObjectSecret
    print('secretSlumObj')

    sSlum=secretSlumObj.obj
    #Si no existe lanzar error
    if sSlum.get('data').get('user') == None:
        print('NoneUserObjectSecret')
        raise NoneUserObjectSecret
    #Si no existe lanzar error
    if sSlum.get('data').get('passwd') == None:
        print('NonePassObjectSecret')
        raise NonePassObjectSecret

    #print(f'XXXXXX> secretName: {sSlum.get("data").get("user")}')
    #print(json.dumps(    secretSlumObj.obj,    sort_keys=True,    indent=4,    separators=(',', ': ')    ))
def executeScriptRemote(host,user,passwd,port,jobName):
    print(host,user,passwd,port,jobName)
    #SBATCH 
    #squeue --format="%.18i %.9P %.30j %.8u %.8T %.10M " -n migagi-1 -t all squeue --format="%.18i;%.9P;%.30j;%.8u;%.8T;%.10M;" -n MIGAGI-JOB -t all
    result = SSH(host=host,user=user,passwd=passwd,port=port).execute('squeue --format="%.18i;%.9P;%.30j;%.8u;%.8T;%.10M;" -n '+jobName+' -t all')
    status=result[2]
    if status == 0:
        return parse_squeue(result[0])
    else : return[]

# This function facilitates the parsing of the squeue command exit
def parse_squeue(out):
    #out = out.decode()
    #if out.find("=") < 0: return []
    r = []
    for line in out.split("\n"):
        split_val = line.replace(' ','').split(';')
        #print(split_val)
        r.append(split_val)
    r.pop(0)
    r.pop()
    return r