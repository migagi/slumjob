FROM python:3.7
LABEL maintainer=migagi@posgrado.upv.es
ARG kopf_version='1.35.4'

#RUN apt-get update && apt-get install -y python3 python3-pip
RUN pip install kopf==${kopf_version}
RUN pip install kopf[full-auth]
RUN pip install pykube-ng
RUN pip install requests
RUN pip install pyyaml
RUN pip3 install kubernetes
RUN pip install certbuilder
RUN pip install paramiko
RUN pip install kopf[dev]
#RUN apt-get update && apt-get install -y --no-install-recommends ntp
RUN command
ADD  SSH.py /home/SSH.py
ADD slurmJob-operator.py /home/slurmJob-operator.py

CMD kopf run /home/slurmJob-operator.py --verbose --liveness=http://0.0.0.0:8080/healthz
