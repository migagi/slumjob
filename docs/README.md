
## SlumJob Opetator

En el presente trabajo se ha desarrollado un operador de kubernetes <sub>(1)</sub> donde se pretende intregrar los trabajos que se lanzan en una cola de trabajos Slum <sub>(2)</sub>.

Para realizar este desarrollo se ha empleado el framework kopf <sub>(3)</sub> y el cliente kubernetes para python pykube-ng <sub>(4)</sub>.

Primer paso ha sido crear el CRD <sub>CustomResourceDefinition</sub> :

```yaml
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: slumjobs.kopf.dev
spec:
  scope: Namespaced
  group: kopf.dev
  names:
    kind: SlumJob
    plural: slumjobs
    singular: slumjob
    shortNames:
      - sljs
      - slj
  versions:
    - name: v1
      served: true
      storage: true
      schema:
        openAPIV3Schema:
          type: object
          properties:
            spec:
              type: object
              x-kubernetes-preserve-unknown-fields: true
              properties:
                name:
                  type: string
                connection:
                  type: object
                  x-kubernetes-preserve-unknown-fields: true
                  properties:
                    host:
                      type: string
                    user:
                      type: string
                    passwd:
                      type: string
                    port:
                      type: string
                job:
                  type: object
                  x-kubernetes-preserve-unknown-fields: true
                  properties:
                    scriptCode:
                      type: string
            status:
              type: object
              x-kubernetes-preserve-unknown-fields: true
      additionalPrinterColumns:
        - name: Age
          type: date
          jsonPath: .metadata.creationTimestamp
        - name: status
          type: string
          description: The cron spec defining the interval a CronJob is run
          jsonPath: .spec.status
```
Explicar para que es cada bloque
spec:
names:
    kind: SlumJob
    plural: slumjobs
    singular: slumjob
    shortNames:

schema
    name: string
    connection:
        host: string
        user: string
        passwd: string
        port: string
    job: object
        scriptCode:
            type: string
status: object
